package tarea3U4.Tarea3U4.Entidades;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class UsuarioTest {

    Usuario usuario;
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        //Crear usuario de prueba
        usuario = new Usuario("John", "Dalton", "Lee", "20.537.746-8", "+56911111111", 26);
    }

    @Test
    public void testCrearUsuario() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonUsuario = objectMapper.writeValueAsString(usuario);

        mockMvc.perform(post("/api/crear")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonUsuario))
                .andExpect(status().isOk())
                .andExpect(content().string("Usuario creado correctamente"));
    }

    @Test
    @DisplayName("Test para validar un nombre")
    public void testValidarNombre() {
        boolean resultado = usuario.validarNombre(usuario.getNombre());
        Assertions.assertTrue(resultado);
    }

    @Test
    @DisplayName("Test para validar un apellido paterno")
    public void testValidarApellidoPaterno(){
        boolean apellidoPat = usuario.validarApellidoPaterno(usuario.getApellido_paterno());
        Assertions.assertTrue(apellidoPat);
    }

    @Test
    @DisplayName("Test para validar un apellido materno")
    public void testValidarApellidoMaterno(){
        boolean apellidoMat = usuario.validarApellidoPaterno(usuario.getApellido_materno());
        Assertions.assertTrue(apellidoMat);
    }

    @Test
    @DisplayName("Test para validar un rut")
    public void testValidarRut(){
        boolean rut = usuario.validarRut(usuario.getRut());
        Assertions.assertTrue(rut);
    }

    @Test
    @DisplayName("Test para validar un numero de telefono")
    public void testValidarNumero(){
        boolean resultado = usuario.validarNumeroTelefonico(usuario.getNumero_telefonico());
        Assertions.assertTrue(resultado);
    }

    @Test
    @DisplayName("Test para validar edad")
    public void testValidarEdad(){
        boolean edad = usuario.validarEdad(usuario.getEdad());
        Assertions.assertTrue(edad);
    }
}