package tarea3U4.Tarea3U4.Controlador;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tarea3U4.Tarea3U4.Entidades.Usuario;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UsuarioControlador {
    @PostMapping("/crear")
    public ResponseEntity<String> crearUsuario(@RequestBody Usuario usuario) {
        //Lista para almacenar los campos no validados
        List<String> errores = new ArrayList<>();

        // Validar el nombre
        if (!usuario.validarNombre(usuario.getNombre())) {
            errores.add("El nombre contiene numeros y/o caracteres invalidos");
        }

        // Validar el apellido paterno
        if (!usuario.validarApellidoPaterno(usuario.getApellido_paterno())) {
            errores.add("Apellido paterno incorrecto");
        }

        // Validar el apellido materno
        if (!usuario.validarApellidoMaterno(usuario.getApellido_materno())) {
            errores.add("Apellido materno incorrecto");
        }

        // Validar el rut
        if (!usuario.validarRut(usuario.getRut())) {
            errores.add("Rut incorrecto");
        }

        // Validar el número telefónico
        if (!usuario.validarNumeroTelefonico(usuario.getNumero_telefonico())) {
            errores.add("El numero de telefonono esta incorrecto");
        }

        // Validar la edad
        if (!usuario.validarEdad(usuario.getEdad())) {
            errores.add("La edad no corresponde");
        }

        if (!errores.isEmpty()) {
            return ResponseEntity.badRequest().body(String.join(", ", errores));
        }

        return ResponseEntity.ok("Usuario creado correctamente");
    }
}
