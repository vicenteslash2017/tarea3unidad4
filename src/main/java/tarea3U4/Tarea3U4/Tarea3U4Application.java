package tarea3U4.Tarea3U4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tarea3U4Application {

	public static void main(String[] args) {
		SpringApplication.run(Tarea3U4Application.class, args);
	}

}
