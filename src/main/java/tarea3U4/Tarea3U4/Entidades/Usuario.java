package tarea3U4.Tarea3U4.Entidades;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Usuario {
    private String nombre;
    private String apellido_paterno;
    private String apellido_materno;
    private String Rut;
    private String numero_telefonico ;
    private int edad;

    public Usuario(String nombre, String apellido_paterno, String apellido_materno, String rut, String numero_telefonico, int edad) {
        this.nombre = nombre;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        Rut = rut;
        this.numero_telefonico = numero_telefonico;
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }
    public String getApellido_paterno(){return apellido_paterno;}
    public String getApellido_materno(){return apellido_materno;}
    public String getRut(){return Rut;}
    public String getNumero_telefonico() {
        return numero_telefonico;
    }
    public int getEdad() {
        return edad;
    }

    // Métodos de validación
    public boolean validarNombre(String nombre) {
        // Se implementa la lógica de validación para el nombre
        Pattern pat = Pattern.compile("^[a-zA-Z]+$");
        Matcher mat = pat.matcher(nombre);

        if(mat.matches()){
            System.out.println("***************");
            System.out.println("Nombre correcto");
            return true;
        }
        else{
            System.out.println("Nombre Incorrecto");
            return false;
        }
    }

    public boolean validarApellidoPaterno(String appellidoPat){
        // Se implementa la lógica de validación de apellido paterno
        Pattern pat = Pattern.compile("^[a-zA-Z]+$");
        Matcher mat = pat.matcher(appellidoPat);

        if(mat.matches()){
            System.out.println("Apellido correcto");
            return true;
        }
        else{
            System.out.println("Apellido Incorrecto");
            return false;
        }

    }

    public boolean validarApellidoMaterno(String appellidoMat){
        // Se implementa la lógica de validación para apellido materno
        Pattern pat = Pattern.compile("^[a-zA-Z]+$");
        Matcher mat = pat.matcher(appellidoMat);

        if(mat.matches()){
            System.out.println("Apellido correcto");
            return true;
        }
        else{
            System.out.println("Apellido Incorrecto");
            return false;
        }

    }

    public boolean validarRut(String rut){
        // Se implementa la lógica de validación para rut
        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }

    public boolean validarNumeroTelefonico(String numero) {
        // Implementa la lógica de validación para el número telefónico
        Pattern pat = Pattern.compile("^\\+?56[2-9]\\d{8}$");
        Matcher mat = pat.matcher(numero);

        if (mat.matches()) {
            System.out.println("Numero correcto");
            return true;
        }
        System.out.println("Número erroneo");
        return false;
    }

    public boolean validarEdad(int edad) {
        // Implementa la lógica de validación para la edad
        if (edad <= 0 || edad > 122){
            System.out.println("Edad no puede ser negativa");
            return false;
        }
        System.out.println("Edad correcta");
        return true;
    }
}
